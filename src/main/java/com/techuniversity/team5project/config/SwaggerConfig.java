package com.techuniversity.team5project.config;

import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.ResponseEntity;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    /**
     * Initializes all the parameter necessary for swagger api
     */

    @Value("${api.info.title}")
    private String title;

    @Value("${api.info.description}")
    private String description;

    @Value("${api.info.version}")
    private String version;

    @Value("${api.info.license}")
    private String license;

    @Value("${api.info.licenseUrl}")
    private String licenseUrl;

    @Value("${api.info.contact.name}")
    private String name;

    @Value("${api.info.contact.url}")
    private String url;

    @Value("${api.info.contact.email}")
    private String email;

    @Value("${api.info.pathMapping}")
    private String pathMapping;

    @Value("${api.info.groupName}")
    private String groupName;


    /**
     * Create Swagger Api Configuration.
     *
     * @return Swagger Docket
     */

    @Bean
    public Docket customImplementation() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName(groupName)
                .apiInfo(metaData())
                .select()
                .apis(RequestHandlerSelectors.withClassAnnotation(Api.class))
                .paths(PathSelectors.any())
                .build()
                .pathMapping(pathMapping)
                .genericModelSubstitutes(ResponseEntity.class)
                .useDefaultResponseMessages(false);
    }

    private ApiInfo metaData() {
        return new ApiInfoBuilder()
                .title(title)
                .description(description)
                .version(version)
                .license(license)
                .licenseUrl(licenseUrl)
                //.contact(new Contact(name, url, email))
                .build();
    }

}
