package com.techuniversity.team5project.Utils;

public enum ErrorMessagesEnum {
    MEDIA_TYPE_NOT_SUPPORTED("media type is not supported. Supported media types are"),
    MALFORMED_JSON("Malformed JSON request"),
    ERROR_WRITING("Error writing JSON output"),
    UNEXPECTED_ERROR("Unexpected error"),
    DATABASE_ERROR("Database error"),
    PARAMETER_MISSING(" parameter is missing"),
    NOT_FOUND_PARAMEATERS(" was not found for parameters ");

    private String message;

    ErrorMessagesEnum(String message) {
        this.message = message;
    }

    public String getMessage() {
        return this.message;
    }
}
