package com.techuniversity.team5project.controllers.interfaces;

import com.techuniversity.team5project.domain.entities.Client;
import com.techuniversity.team5project.model.ApiError;
import com.techuniversity.team5project.model.ClientRequest;
import com.techuniversity.team5project.model.ClientResponse;
import com.techuniversity.team5project.model.ContractResponse;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

public interface ContractController {


    @ApiOperation(
            notes = "Method to get info related to main panel",
            value = "global main welcome panel",
            response = ContractResponse.class
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful operation"),
            @ApiResponse(code = 204, message = "No Content"),
            @ApiResponse(code = 500, message = "Server error", response = ApiError.class)
    })
    ContractResponse getContract(@RequestParam String dni);

    @ApiOperation(
            notes = "Method to get info related to main panel",
            value = "global main welcome panel",
            response = ContractResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful operation"),
            @ApiResponse(code = 500, message = "Server error", response = ApiError.class)
    })
    ContractResponse createContract(@RequestBody ClientRequest clientRequest);

    @ApiOperation(
            notes = "Method to get info related to main panel",
            value = "global main welcome panel",
            response = ContractResponse.class
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful operation"),
            @ApiResponse(code = 500, message = "Server error", response = ApiError.class)
    })

    void deleteClient(@RequestParam String dni);

    @ApiOperation(
            notes = "Method to get info related to main panel",
            value = "global main welcome panel",
            response = ClientResponse.class
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful operation"),
            @ApiResponse(code = 500, message = "Server error", response = ApiError.class)
    })
    ClientResponse updateClient(@RequestBody ClientRequest clientRequest);

    /* @ApiOperation(
            notes = "Method to get info related to main panel",
            value = "global main welcome panel",
            response = MainResponse.class
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful operation"),
            @ApiResponse(code = 500, message = "Server error", response = ApiError.class)
    })
    List<MainResponse> getAll();*/

    /*

    @ApiOperation(
            notes = "Method to get info related to main panel",
            value = "global main welcome panel",
            response = MainResponse.class
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful operation"),
            @ApiResponse(code = 500, message = "Server error", response = ApiError.class)
    })

    void deleteEntity(@PathVariable(value = "mainId") String mainId);

    @ApiOperation(
            notes = "Method to get info related to main panel",
            value = "global main welcome panel",
            response = MainResponse.class
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful operation"),
            @ApiResponse(code = 500, message = "Server error", response = ApiError.class)
    })
    MainResponse createEntity (@RequestBody MainRequest mainRequest);*/

}
