package com.techuniversity.team5project.repositories;

import com.techuniversity.team5project.domain.entities.Client;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientRepository extends MongoRepository<Client, String> {

    Client findByDni(String dni);
    Client save(Client client);
    void deleteByDni(String dni);
}
