package com.techuniversity.team5project.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ContractResponse {
    private String name;
    private String surname;
    private String address;
    private int phoneNumber;
    private String bank;
    private String branch;
    private String counterpart;
    private int sheet;

}
