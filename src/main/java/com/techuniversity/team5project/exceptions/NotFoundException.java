package com.techuniversity.team5project.exceptions;

public class NotFoundException extends BaseException {

    public NotFoundException(String message) {
        super(message);
    }
}